package com.colorata.pomogator.architecture

import androidx.glance.GlanceModifier
import androidx.glance.ImageProvider
import androidx.glance.action.actionParametersOf
import androidx.glance.appwidget.action.actionRunCallback
import androidx.glance.background
import com.colorata.pomogator.*

object Finalizer {
    fun switch() =
        if (!Parameters.isReset) actionRunCallback<ResetTime>(
            actionParametersOf(
                AppWidget.currentTimeKey to Parameters.time,
                AppWidget.isResetKey to Parameters.isReset
            )
        ) else actionRunCallback<StartTimer>(
            actionParametersOf(
                AppWidget.currentTimeKey to Parameters.time,
                AppWidget.isResetKey to Parameters.isReset
            )
        )

    fun icon() =
        if (Parameters.isReset) R.drawable.ic_baseline_play_arrow_24 else R.drawable.ic_baseline_replay_24

    fun slashBar() = if (Parameters.isChill) 3f else 15f

    fun colorBar() =
        if (Parameters.isChill) Parameters.tertiaryLightColor else Parameters.accentColor

    fun widgetBackground(rounded: Boolean = false) =
        GlanceModifier.background(ImageProvider(if (!rounded) R.drawable.appwidget_background else R.drawable.appwidget_background_rounded))

    fun itemBackground(rounded: Boolean = false) =
        GlanceModifier.background(ImageProvider(if (!rounded) R.drawable.appwidget_inner else R.drawable.appwidget_inner_rounded))
}
