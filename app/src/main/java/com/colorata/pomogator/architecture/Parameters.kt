package com.colorata.pomogator.architecture

import android.content.Context
import android.os.Build
import androidx.compose.ui.unit.DpSize
import androidx.compose.ui.unit.dp
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.floatPreferencesKey
import com.colorata.pomogator.AppWidget
import com.colorata.pomogator.R
import com.colorata.pomogator.androidColor

object Parameters {
    var time = 0f
    var isReset = true
    var isChill = false
    var backgroundColor = -1
    var accentColor = -1
    var tertiaryLightColor = -1
    var tertiaryDarkColor = -1

    object Sizes {
        private const val defaultWidth = 75
        private const val defaultHeight = 100


        val smallSquare = DpSize((defaultWidth).dp, (defaultHeight).dp)
        val square = DpSize((defaultWidth * 2).dp, (defaultHeight * 2).dp)
        val horizontalLine = DpSize((defaultWidth * 2).dp, (defaultHeight).dp)
        val bigHorizontalLine = DpSize((defaultWidth * 4).dp, (defaultHeight).dp)
        val verticalLine = DpSize((defaultWidth).dp, (defaultHeight * 2).dp)
        val bigVerticalLine = DpSize((defaultWidth).dp, (defaultHeight * 4).dp)
        val horizontalBigBox = DpSize((defaultWidth * 4).dp, (defaultHeight * 2).dp)
        val verticalBigBox = DpSize((defaultWidth * 2).dp, (defaultHeight * 4).dp)
        val bigSquare = DpSize((defaultWidth * 3).dp, (defaultHeight * 2).dp)
    }

    fun init(prefs: Preferences, context: Context) {
        time = prefs[floatPreferencesKey(AppWidget.currentTimeKey.name)] ?: 0f
        isReset = prefs[booleanPreferencesKey(AppWidget.isResetKey.name)] ?: true
        isChill = prefs[booleanPreferencesKey(AppWidget.isChillKey.name)] ?: false
        backgroundColor = context.androidColor(android.R.attr.colorBackground)
        accentColor = context.androidColor(android.R.attr.colorAccent)
        tertiaryLightColor =
            context.getColor(R.color.tertiary_light)
        tertiaryDarkColor =
            context.getColor(R.color.tertiary_dark)
    }
}