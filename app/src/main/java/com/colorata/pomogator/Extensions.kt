package com.colorata.pomogator

import android.R.attr.bitmap
import android.content.Context
import android.graphics.*
import android.os.Build
import android.util.TypedValue
import android.view.ContextThemeWrapper
import androidx.compose.runtime.Composable
import androidx.compose.ui.unit.dp
import androidx.glance.GlanceModifier
import androidx.glance.appwidget.cornerRadius
import java.util.*


fun Context.androidColor(path: Int): Int {
    val typedValue = TypedValue()
    val contextThemeWrapper = ContextThemeWrapper(
        this,
        if (Build.VERSION.SDK_INT >= 29)
            android.R.style.Theme_DeviceDefault_DayNight else android.R.style.Theme_DeviceDefault
    )
    contextThemeWrapper.theme.resolveAttribute(
        path,
        typedValue, true
    )
    return typedValue.data
}

@Composable
fun GlanceModifier.appWidgetBackgroundRadius(): GlanceModifier {
    return if (Build.VERSION.SDK_INT >= 31) {
        this.cornerRadius(android.R.dimen.system_app_widget_background_radius)
    } else {
        this.cornerRadius(16.dp)
    }
}

fun createCircleProgressBar(i: Float, color: Int): Bitmap {
    val bitmap = Bitmap.createBitmap(300, 300, Bitmap.Config.ARGB_8888)
    val canvas = Canvas(bitmap)
    val paint = Paint()
    canvas.scale(-1f, 1f, 150f, 150f)
    paint.color = color
    paint.strokeWidth = 20f
    paint.strokeCap = Paint.Cap.ROUND
    paint.style = Paint.Style.FILL
    val oval = RectF()
    paint.style = Paint.Style.STROKE
    oval[10f, 10f, 290f] = 290f
    canvas.drawArc(oval, 270f, i * 360f / 100, false, paint)
    return bitmap
}

fun getTime(): Calendar = Calendar.getInstance()

fun GlanceModifier.appWidgetCornerRadius() =
    if (Build.VERSION.SDK_INT >= 31) cornerRadius(android.R.dimen.system_app_widget_background_radius)
    else cornerRadius(16.dp)

fun drawSlider(foregroundColor: Int, backgroundColor: Int, percentage: Float): Bitmap {
    val bitmap = Bitmap.createBitmap(500, 100, Bitmap.Config.ARGB_8888)
    val canvas = Canvas(bitmap)
    canvas.drawRoundRect(RectF(0f, 0f, 500f, 100f), 50f, 50f, Paint().apply {
        color = backgroundColor
    })

    canvas.drawRoundRect(RectF(0f, 0f, percentage * 5f, 100f), 50f, 50f, Paint().apply {
        color = foregroundColor
    })
    return bitmap
}
