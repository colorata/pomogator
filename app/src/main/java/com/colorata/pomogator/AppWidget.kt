package com.colorata.pomogator

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.datastore.preferences.core.MutablePreferences
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.floatPreferencesKey
import androidx.glance.*
import androidx.glance.action.ActionParameters
import androidx.glance.appwidget.GlanceAppWidget
import androidx.glance.appwidget.GlanceAppWidgetReceiver
import androidx.glance.appwidget.SizeMode
import androidx.glance.appwidget.action.ActionCallback
import androidx.glance.appwidget.appWidgetBackground
import androidx.glance.appwidget.state.updateAppWidgetState
import androidx.glance.layout.Alignment
import androidx.glance.layout.Box
import androidx.glance.layout.fillMaxSize
import androidx.glance.state.GlanceStateDefinition
import androidx.glance.state.PreferencesGlanceStateDefinition
import androidx.glance.text.Text
import com.colorata.pomogator.AppWidget.Companion.currentTimeKey
import com.colorata.pomogator.AppWidget.Companion.isChillKey
import com.colorata.pomogator.AppWidget.Companion.isResetKey
import com.colorata.pomogator.architecture.Parameters
import com.colorata.pomogator.ui.widgets.SmallRow
import com.colorata.pomogator.ui.widgets.SmallSquare
import kotlinx.coroutines.*

const val ACTION_STOP = "${BuildConfig.APPLICATION_ID}.stop"
private var isUse by mutableStateOf(false)


class AppWidget : GlanceAppWidget() {

    override val stateDefinition: GlanceStateDefinition<*> = PreferencesGlanceStateDefinition
    override val sizeMode = SizeMode.Responsive(
        mutableSetOf(
            Parameters.Sizes.smallSquare,
            Parameters.Sizes.square,
            Parameters.Sizes.horizontalLine,
            Parameters.Sizes.verticalLine,
            Parameters.Sizes.bigSquare,
            Parameters.Sizes.bigHorizontalLine,
            Parameters.Sizes.bigVerticalLine,
            Parameters.Sizes.horizontalBigBox,
            Parameters.Sizes.verticalBigBox
        )
    )

    @SuppressLint("ResourceType")
    @Composable
    override fun Content() {
        val size = LocalSize.current
        Parameters.init(currentState(), LocalContext.current)
        when (size) {
            Parameters.Sizes.smallSquare -> SmallSquare()
            Parameters.Sizes.horizontalLine -> SmallRow()
            else -> {
                Box(
                    modifier = GlanceModifier.fillMaxSize()
                        .background(ImageProvider(R.drawable.appwidget_background))
                        .appWidgetBackground(),
                    contentAlignment = Alignment.Center
                ) {
                    Text(
                        text = "${size.width.value.toInt()} ${size.height.value.toInt()} is not yet implemented"
                    )
                }
            }
        }
    }

    companion object {

        val isChillKey = ActionParameters.Key<Boolean>("isChill")
        val isResetKey = ActionParameters.Key<Boolean>("isReset")
        val currentTimeKey = ActionParameters.Key<Float>("currentTime")

    }
}

class WidgetReceiver : GlanceAppWidgetReceiver() {

    override val glanceAppWidget: GlanceAppWidget
        get() = AppWidget()
}

class ResetTime : ActionCallback {
    @RequiresApi(Build.VERSION_CODES.O)
    override suspend fun onRun(context: Context, glanceId: GlanceId, parameters: ActionParameters) {
        val widget = WidgetState(context, PreferencesGlanceStateDefinition, glanceId)
        widget.update {
            this[floatPreferencesKey(currentTimeKey.name)] = 0f
            this[booleanPreferencesKey(isResetKey.name)] = true
            this[booleanPreferencesKey(isChillKey.name)] = false
            isUse = false
        }

        context.startForegroundService(Intent(context, ForegroundService::class.java).apply {
            action = ACTION_STOP
        })
    }
}

class StartTimer : ActionCallback {
    @RequiresApi(Build.VERSION_CODES.O)
    override suspend fun onRun(context: Context, glanceId: GlanceId, parameters: ActionParameters) {
        context.startForegroundService(Intent(context, ForegroundService::class.java))

        val widget = WidgetState(context, PreferencesGlanceStateDefinition, glanceId)

        widget.update {
            this[floatPreferencesKey(currentTimeKey.name)] = 1500f
            this[booleanPreferencesKey(isResetKey.name)] = false
            this[booleanPreferencesKey(isChillKey.name)] = false
        }
        CoroutineScope(Dispatchers.IO).launch {
            isUse = true
            for (i in 0 until 1500) {
                if (!isUse) break
                widget.update {
                    val localTime = this[floatPreferencesKey(currentTimeKey.name)]
                    if (localTime != null) {
                        this[floatPreferencesKey(currentTimeKey.name)] = localTime - 1f
                    }
                }
                delay(1000)
            }
            if (isUse) {
                widget.update {
                    this[floatPreferencesKey(currentTimeKey.name)] = 300f
                    this[booleanPreferencesKey(isChillKey.name)] = true
                }
            }
            for (i in 0 until 300) {
                if (!isUse) break
                widget.update {
                    val localTime = this[floatPreferencesKey(currentTimeKey.name)]
                    if (localTime != null) {
                        this[floatPreferencesKey(currentTimeKey.name)] = localTime - 1f
                    }
                }
                delay(1000)
            }
            widget.update {
                this[booleanPreferencesKey(isResetKey.name)] = true
                this[floatPreferencesKey(currentTimeKey.name)] = 0f
            }
        }
    }
}

fun WidgetState.update(
    content: MutablePreferences.() -> Unit
) {
    CoroutineScope(Dispatchers.IO).launch {
        withContext(Dispatchers.Main) {
            updateAppWidgetState(
                context = context,
                preferencesGlanceStateDefinition,
                glanceId = glanceId
            ) {
                it.toMutablePreferences().apply {
                    content()
                }
            }
            AppWidget().update(context = context, glanceId = glanceId)
        }
    }
}

data class WidgetState(
    val context: Context,
    val preferencesGlanceStateDefinition: GlanceStateDefinition<Preferences>,
    val glanceId: GlanceId
)