package com.colorata.pomogator.ui.widgets

import androidx.compose.runtime.Composable
import androidx.compose.ui.unit.dp
import androidx.glance.GlanceModifier
import androidx.glance.Image
import androidx.glance.ImageProvider
import androidx.glance.action.clickable
import androidx.glance.appwidget.appWidgetBackground
import androidx.glance.appwidget.cornerRadius
import androidx.glance.layout.Alignment
import androidx.glance.layout.Box
import androidx.glance.layout.padding
import androidx.glance.layout.size
import com.colorata.pomogator.architecture.Finalizer
import com.colorata.pomogator.architecture.Parameters
import com.colorata.pomogator.createCircleProgressBar

@Composable
fun SmallSquare() {
    Box(
        modifier = GlanceModifier.clickable(
            Finalizer.switch()
        ),
        contentAlignment = Alignment.Center
    ) {

        Image(
            provider = ImageProvider(
                bitmap = createCircleProgressBar(
                    Parameters.time / Finalizer.slashBar(),
                    Finalizer.colorBar()
                )
            ),
            contentDescription = null,
            modifier = Finalizer.widgetBackground(true)
                .appWidgetBackground()
                .cornerRadius(50.dp)
                .size(75.dp)
        )
        Box(
            modifier = GlanceModifier.padding(10.dp),
            contentAlignment = Alignment.Center
        ) {
            Image(
                provider = ImageProvider(Finalizer.icon()),
                contentDescription = null
            )
        }
    }
}